data aws_caller_identity caller_identity {
  provider = "aws"
}

locals {
  account_id = "${data.aws_caller_identity.caller_identity.account_id}"
}

resource s lambda_function {
  function_name    = "data-extracts"
  runtime          = "python2.7"
  handler          = "src/index.refresh"
  role             = "arn:aws:iam::${local.account_id}:role/DataExtractsRole"
  filename         = "${var.artefact}"
  source_code_hash = "${base64sha256(file("${var.artefact}"))}"
  timeout          = 30

  environment {
    variables = {
      REGION = "${var.region}"
    }
  }
}
