provider aws {
  version     = "1.13"
  region      = "eu-west-1"
  assume_role = ["${var.workspace_iam_roles[terraform.workspace]}"]
}
