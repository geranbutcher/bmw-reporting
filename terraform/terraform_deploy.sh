#!/usr/bin/env bash

env=$1
artefact=$2

terraform init
terraform workspace new ${env}
terraform workspace select ${env}
terraform apply -auto-approve -var "artefact=${artefact}"
