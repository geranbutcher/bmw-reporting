terraform {
  backend s3 {
    bucket = "tfstate-deployment.wrisk.co"
    key    = "cache-refresh.tfstate"
    region = "eu-west-1"
  }
}
