variable workspace_iam_roles {
  type = "map"

  default = {
    reporting = {
      role_arn = "arn:aws:iam::671408338341:role/DeploymentRole"
    }
  }
}

variable region {
  default = "eu-west-1"
}

variable artefact {}
