#!/usr/bin/env bash

set -e

archiveVersion="0.11.5"
archiveName="terraform_${archiveVersion}_linux_amd64.zip"

currentVersion=$( terraform version | head -n 1 || true )
echo ${currentVersion}
if [  "${currentVersion}" != "Terraform v${archiveVersion}" ]; then
    wget -c -P downloads https://releases.hashicorp.com/terraform/${archiveVersion}/${archiveName}
    unzip -o downloads/${archiveName} -d ~/bin
fi
