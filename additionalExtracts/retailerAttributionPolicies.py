from helpers.hhClient import *
from pprint import pprint
from helpers.premiumCalculation import *
from helpers.saveDictToFile import *
import pymongo
import csv

def getRetailerAttributionMapping():
    policies = get7DayPolicies()
    aggregatedData = []
    for policy in policies:
        rowData = {}
        rowData['policy_id'] = policy['quoteReferenceId']
        rowData['retailer_id'] = policy['modelData']['productData']['voucherCode']
        aggregatedData.append(rowData)
    aggregatedData = [i for n, i in enumerate(aggregatedData) if i not in aggregatedData[n + 1:]]
    return aggregatedData

def generateRetailerAttributionPolicyExtract():
    attribution = getRetailerAttributionMapping()
    saveDictToFile(attribution,'additionalExtracts/extracts/retailerAttributionPolicy')
    print('Policy Retailer Attribution Extract Complete')
