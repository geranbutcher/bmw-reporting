from helpers.hhClient import *
from pprint import pprint
from helpers.premiumCalculation import *
from helpers.saveDictToFile import *
import pymongo
import csv

def getLegalPolicyTransactions():
    aggregatedData = []
    inceptions = getLegalPoliciesIncepted()
    for inception in inceptions:
        rowData = {}
        rowData['transactionDate'] = inception['eventData']['createdDate']
        rowData['totalPremium'] = 25
        rowData['grossPremium'] = 25 - calcIPT(25)
        rowData['ipt'] = calcIPT(25)
        rowData['commission'] = 25 - 0.8 - calcIPT(25)
        rowData['arcLegalPremium'] = 0.8
        aggregatedData.append(rowData)
    cancellations = getLegalPoliciesCancelled()
    for cancellation in cancellations:
        rowData = {}
        rowData['transactionDate'] = cancellation['eventData']['createdDate']
        rowData['totalPremium'] = -25
        rowData['grossPremium'] = -25 + calcIPT(25)
        rowData['ipt'] = calcIPT(25) * -1
        rowData['commission'] = -25 - -0.8 + calcIPT(25)
        rowData['arcLegalPremium'] = -0.8
        aggregatedData.append(rowData)
    return aggregatedData

def generateLegalCoverExtract():
    attribution = getLegalPolicyTransactions()
    saveDictToFile(attribution,'additionalExtracts/extracts/legalcover')
    print('Legal Cover Extract Complete')
