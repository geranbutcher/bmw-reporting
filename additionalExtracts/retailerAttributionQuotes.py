from helpers.hhClient import *
from pprint import pprint
from helpers.premiumCalculation import *
from helpers.saveDictToFile import *
import pymongo
import csv

def getRetailerAttributionMapping():
    policies = getQuotes()
    aggregatedData = []
    for policy in policies:
        rowData = {}
        rowData['quote_id'] = policy['quoteReferenceId']
        try:
            rowData['retailer_id'] = policy['modelData']['productData']['voucherCode']
        except:
            rowData['retailer_id'] = None
        aggregatedData.append(rowData)
    aggregatedData = [i for n, i in enumerate(aggregatedData) if i not in aggregatedData[n + 1:]]
    return aggregatedData

def generateRetailerAttributionQuoteExtract():
    attribution = getRetailerAttributionMapping()
    saveDictToFile(attribution,'additionalExtracts/extracts/retailerAttributionQuote')
    print('Quote Retailer Attribution Extract Complete')
