from helpers.hhClient import *
from pprint import pprint
from helpers.premiumCalculation import *
from helpers.saveDictToFile import *
from datetime import datetime
import pymongo
import csv

def quoteDetails():
    quotes = getQuotes()
    allQuoteDetails = []
    for quote in quotes:
        eventDetails = {}
        eventDetails['quote_id'] = quote['quoteReferenceId']
        eventDetails['EmailConsent'] = quote['modelData']['personalDetails']['communicationSettings']['byEmail']
        eventDetails['First Name'] = quote['modelData']['personalDetails']['firstName']
        eventDetails['Last Name'] = quote['modelData']['personalDetails']['lastName']
        eventDetails['Email'] = quote['modelData']['personalDetails']['email']
        eventDetails['postcode'] = quote['modelData']['personalDetails']['address']['postcode']
        if quote['status'] == 'UnderWritten':
            eventDetails['Converted'] = True
        else:
            eventDetails['Converted'] = False
        eventDetails['status'] = quote['modelData']['personalDetails']['address']['postcode']
        allQuoteDetails.append(eventDetails)
    allQuoteDetails = [i for n, i in enumerate(allQuoteDetails) if i not in allQuoteDetails[n + 1:]]
    return allQuoteDetails

def getAllquotes(collection):
    quotes = []
    quotes = quoteDetails(getquotes(collection))
    return quotes

def generateCustomerOptInsOuts():
    transactions = quoteDetails()
    saveDictToFile(transactions,'additionalExtracts/extracts/customerOptInsOuts')
    print('Customer Opt Ins and Outs Extract Complete')
