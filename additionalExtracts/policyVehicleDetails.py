from helpers.hhClient import *
from pprint import pprint
from helpers.premiumCalculation import *
from helpers.saveDictToFile import *
from datetime import *


def policyDetails():
    policies = getPolicyInceptions()
    allPolicyDetails = []
    for policy in policies:
        eventDetails = {}
        eventDetails['policy_id'] = policy['eventData']['quoteReferenceId']
        eventDetails['make'] = policy['eventData']['modelData']['productData']['additionalCarDetails']['vehicleManufacturer']['description']
        try:
            eventDetails['reg_date'] = str(datetime.strptime(policy['eventData']['modelData']['productData']['additionalCarDetails']['registrationDate'],  '%d %B %Y'))
        except:
            eventDetails['reg_date'] = str(datetime(int(policy['eventData']['modelData']['productData']['additionalCarDetails']['vehicleManufacturedYear']),1,1))
        try:
            eventDetails['retailer'] = policy['eventData']['modelData']['productData']['voucherCode']
        except:
            eventDetails['retailer'] = None
        allPolicyDetails.append(eventDetails)
    allPolicyDetails = [i for n, i in enumerate(allPolicyDetails) if i not in allPolicyDetails[n + 1:]]
    return allPolicyDetails

def generatePolicyVehicleDetailsExtract():
    transactions = policyDetails()
    saveDictToFile(transactions,'transactions/extracts/policymakes')
    print('Vehicle Details Extract Complete')
