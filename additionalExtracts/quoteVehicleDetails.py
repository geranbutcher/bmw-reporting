from helpers.hhClient import *
from pprint import pprint
from helpers.premiumCalculation import *
from helpers.saveDictToFile import *
from datetime import datetime
import pymongo
import csv

def quoteDetails():
    quotes = getQuotes()
    allQuoteDetails = []
    for quote in quotes:
        eventDetails = {}
        eventDetails['quote_id'] = quote['quoteReferenceId']
        eventDetails['make'] = quote['modelData']['productData']['additionalCarDetails']['vehicleManufacturer']['description']
        try:
            eventDetails['reg_date'] = str(datetime.strptime(quote['modelData']['productData']['additionalCarDetails']['registrationDate'],  '%d %B %Y'))
        except:
            eventDetails['reg_date'] = str(datetime(int(quote['modelData']['productData']['additionalCarDetails']['vehicleManufacturedYear'][:4]),1,1))
        try:
            eventDetails['model'] = quote['modelData']['productData']['additionalCarDetails']['vehicleLookupInfo']['displayName']
        except:
            eventDetails['model'] = None
        try:
            eventDetails['retailer'] = quote['modelData']['productData']['voucherCode']
        except:
            eventDetails['retailer'] = None
        allQuoteDetails.append(eventDetails)
    allQuoteDetails = [i for n, i in enumerate(allQuoteDetails) if i not in allQuoteDetails[n + 1:]]
    return allQuoteDetails

def getAllquotes(collection):
    quotes = []
    quotes = quoteDetails(getquotes(collection))
    return quotes

def generateQuoteVehicleDetails():
    transactions = quoteDetails()
    saveDictToFile(transactions,'transactions/extracts/quotemakes')
    print('Vehicle Details Extract Complete')
