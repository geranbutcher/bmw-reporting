from helpers.hhClient import *
from pprint import pprint
from helpers.premiumCalculation import *
from helpers.saveDictToFile import *
from datetime import datetime
import pymongo
import csv

def quoteDetails():
    quotes = getQuotes()
    allQuoteDetails = []
    for quote in quotes:
        eventDetails = {}
        eventDetails['quote_id'] = quote['quoteReferenceId']
        eventDetails['7Day_Upsell_quote_id'] = quote['quoteReferenceId'] + '/1'
        eventDetails['make'] = quote['modelData']['productData']['additionalCarDetails']['vehicleManufacturer']['description']
        try:
            eventDetails['reg_date'] = str(datetime.strptime(quote['modelData']['productData']['additionalCarDetails']['registrationDate'],  '%d %B %Y'))
        except:
            eventDetails['reg_date'] = str(datetime(int(quote['modelData']['productData']['additionalCarDetails']['vehicleManufacturedYear'][:4]),1,1))
        try:
            eventDetails['model'] = quote['modelData']['productData']['additionalCarDetails']['vehicleLookupInfo']['displayName']
        except:
            eventDetails['model'] = None
        try:
            eventDetails['retailer'] = quote['modelData']['productData']['voucherCode']
        except:
            eventDetails['retailer'] = None
        try:
            eventDetails['VehicleLengthDriven'] = datetime.utcnow() - quote['modelData']['productData']['purchaseDate'].days/float(365) / 12
        except:
            eventDetails['VehicleLengthDriven'] = 0
        try:
            eventDetails['NumberOfNamedDrivers'] = len(quote['modelData']['productData']['namedDrivers']) + 1
        except:
            eventDetails['NumberOfNamedDrivers'] = 1
        try:
            eventDetails['VehicleGroup'] = quote['modelData']['productData']['additionalCarDetails']['vehicleLookupInfo']['value']
        except:
            eventDetails['VehicleGroup'] = None
        try:
            eventDetails['Modifications'] = len(quote['modelData']['productData']['carModificationDetails'])
        except:
            eventDetails['Modifications'] = 0
        eventDetails['driverLicenceYear'] = quote['modelData']['personalDetails']['additionalPersonalDetails']['drivingLicenceYear']['description']
        eventDetails['EmploymentStatus'] = quote['modelData']['personalDetails']['employmentType']['description']
        eventDetails['DaysToInception'] = (quote['modelData']['desiredStartDate'] - quote['updatedDate']).days
        eventDetails['UKResidency'] = quote['modelData']['personalDetails']['residency']['description']
        eventDetails['DateOfBirth'] = quote['modelData']['personalDetails']['dateOfBirth']
        eventDetails['PropertyOwnership']  = quote['modelData']['productData']['propertyOwnership']
        eventDetails['ClaimCount']= len(quote['modelData']['productData']['claimDetails'])
        eventDetails['PostCode'] = quote['modelData']['personalDetails']['address']['postcode']
        eventDetails['DrivingLicenceType'] = quote['modelData']['personalDetails']['additionalPersonalDetails']['drivingLicence']['description']
        eventDetails['NCBYears'] = quote['modelData']['productData']['additionalPolicyDetails']['policyNoClaimsBonus']['description']
        eventDetails['Mileage'] = quote['modelData']['productData']['milesPerYear']
        allQuoteDetails.append(eventDetails)
    allQuoteDetails = [i for n, i in enumerate(allQuoteDetails) if i not in allQuoteDetails[n + 1:]]
    return allQuoteDetails

def getAllquotes(collection):
    quotes = []
    quotes = quoteDetails(getquotes(collection))
    return quotes

def generateCustomerInformationExtract():
    transactions = quoteDetails()
    saveDictToFile(transactions,'additionalExtracts/extracts/customerInformation')
    print('Customer Information Extract Complete')
