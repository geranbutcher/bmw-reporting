from helpers.hhClient import *
from helpers.saveDictToFile import *

funnel = []

def quoteCollection():
    db = getDB()
    collection = db['quotes']
    return collection

def policyCollection():
    db = getDB()
    collection = db['policies']
    return collection

def getTotalQuoteCount():
    collection = quoteCollection()
    count = collection.count({})
    stage = '1 - Quote Started'
    addToFunnel(stage, count)
    return count

def getFirstScreenQuoteCount():
    collection = quoteCollection()
    count = collection.count({'modelData.productData.carValue': { "$ne": None} })
    stage = '2 - Vehicle Details Completed'
    addToFunnel(stage, count)
    return count

def getSecondScreenQuoteCount():
    collection = quoteCollection()
    count = collection.count({'modelData.personalDetails.firstName': { "$ne": None} })
    stage = '3 - Personal Details Completed'
    addToFunnel(stage, count)
    return count

def getThirdScreenQuoteCount():
    collection = quoteCollection()
    count = collection.count({'modelData.productData.additionalPolicyDetails.policyNoClaimsBonus.description': { "$ne": None} })
    stage = '4 - Driver History Completed'
    addToFunnel(stage, count)
    return count

def getFourthScreenQuoteCount():
    collection = quoteCollection()
    count = collection.count({ "$or": [{'status':'UnderWritten'}, {'status':'Completed'}]})
    stage = '5 - Additional Drivers Completed'
    addToFunnel(stage, count)
    return count

def getBoundPolicies():
    collection = policyCollection()
    count = collection.count({'productType':'CarWrisk'})
    stage = '6 - Converted'
    addToFunnel(stage, count)
    return count

def addToFunnel(stage, count):
    funnelRow = {}
    funnelRow['Stage'] = stage
    funnelRow['value'] = count
    funnel.append(funnelRow)


def generateFunnel():
    getTotalQuoteCount()
    getFirstScreenQuoteCount()
    getSecondScreenQuoteCount()
    getThirdScreenQuoteCount()
    getFourthScreenQuoteCount()
    getBoundPolicies()
    saveDictToFile(funnel, 'additionalExtracts/extracts/funnel')
    print('Funnel Extract Complete')
