from helpers.hhClient import *
from pprint import pprint
from helpers.premiumCalculation import *
from helpers.saveDictToFile import *
import pymongo
import csv

def getRetailerAttributionMappingPolicies():
    policies = getPolicyInceptions()
    aggregatedData = []
    for policy in policies:
        rowData = {}
        rowData['policy_id'] = policy['eventData']['quoteReferenceId']
        rowData['agent_id'] = policy['eventData']['actingUserId']
        rowData['phone_no'] = policy['eventData']['modelData']['personalDetails']['phones'][0]['phone']
        aggregatedData.append(rowData)
    aggregatedData = [i for n, i in enumerate(aggregatedData) if i not in aggregatedData[n + 1:]]
    return aggregatedData

def getRetailerAttributionMappingQuotes():
    policies = getQuotes()
    aggregatedData = []
    for policy in policies:
        rowData = {}
        rowData['quote_id'] = policy['quoteReferenceId']
        rowData['agent_id'] = policy['actingUserId']
        aggregatedData.append(rowData)
    aggregatedData = [i for n, i in enumerate(aggregatedData) if i not in aggregatedData[n + 1:]]
    return aggregatedData

def getAgentDetails():
    agents = getAgents()
    allAgentDetails = []
    for agent in agents:
        x = 0
        eventDetails = {}
        eventDetails['agentid'] = agent['_id']
        eventDetails['agent_name'] = agent['userName']
        allAgentDetails.append(eventDetails)
    allAgentDetails = [i for n, i in enumerate(allAgentDetails) if i not in allAgentDetails[n + 1:]]
    return allAgentDetails

def generateAgentExtracts():
    attributionPolicies = getRetailerAttributionMappingPolicies()
    attributionQuotes = getRetailerAttributionMappingQuotes()
    agentDetails = getAgentDetails()
    saveDictToFile(attributionPolicies,'additionalExtracts/extracts/agentAttributionPolicies')
    saveDictToFile(attributionQuotes,'additionalExtracts/extracts/agentAttributionQuotes')
    saveDictToFile(agentDetails,'additionalExtracts/extracts/agentDetails')
    print('Agent Extracts Complete')
