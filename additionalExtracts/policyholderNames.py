from helpers.hhClient import *
from helpers.saveDictToFile import *
import pymongo
import csv
from collections import OrderedDict


def generatePolicyHolderNamesExtract():
    policies = getPolicies()
    listOfNames = []
    for policy  in policies:
        row = {}
        row['ref'] = policy['policyReferenceId']
        row['name'] = policy['modelData']['personalDetails']['firstName'].upper() + " " + policy['modelData']['personalDetails']['lastName'].upper()
        listOfNames.append(row)
    listOfNames = [i for n, i in enumerate(listOfNames) if i not in listOfNames[n + 1:]]
    saveDictToFile(listOfNames,'transactions/extracts/names')
    print('Policyholder Names Extract Complete')
