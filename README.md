* Setup

In order to run these reports you'll need download the scripts from git.

```
brew install git
git clone https://geranbutcher@bitbucket.org/geranbutcher/bmw-reporting.git 

```

Once you've done this, you'll need to download the dependencies for the script.

```
brew install pip
pip install requirements
```

Once you've done this. You'll be ready to run reports!

* Reports

Navigate to the directory you've installed the scripts to. Once their you can
run the following scripts.

** BMW Reports
`python bmwReports.py`
This report provides MI for the BMW REPORTING 2 Tableau workbook.

** BMW Reconciliation
`python bmwReconciliation.py`
These extracts provide data for reconciliation purposes from Hughub and Stripe.

** BMW BDX
`python bmwBDX.py`ta
This extract provides data for the BMW bdx.
