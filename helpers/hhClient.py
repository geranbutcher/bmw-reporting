import pymongo
from pymongo import *
from datetime import datetime
from pprint import pprint

# UAT environment
# def hhClient():
#     client = MongoClient('ds125388.mlab.com:25388',
#                         username='wrisk_reporting2',
#                         password='Nh4ig61nv6rb',
#                         authSource='hughub-wriskuat',
#                         # authMechanism='SCRAM-SHA-1'
#                         )
#     return client

# Prod environment
def hhClient():
    client = MongoClient('ds042077-a0.mlab.com:42077',
                        username='admin',
                        password='Y3gNH8J*EsgTCBxs',
                        authSource='hug-wrisk',
                        # authMechanism='SCRAM-SHA-256'
                        )
    return client

start = None
end = None

def getDB():
    client = hhClient()
    db = client['hug-wrisk']
    # db = client['hughub-wris kuat']
    return db

def setStartDate(day,month,year):
    global start
    start = datetime(year,month,day,0,0,0)
    print('Start Datetime: ' + str(start))

def setEndDate(day,month,year):
    global end
    end = datetime(year,month,day,0,0,0)
    print('End Datetime: ' + str(end))


def getPolicies():
    db = getDB()
    collection = db['policies']
    policies = collection.find({ "$and": [{'status': {'$ne': 'Superceded'}, 'productType':'CarWrisk', 'createdDate': {'$gte': start,'$lte': end}}]})
    print('Processing ' + str(policies.count()) + ' Policies')
    policiesConverted = convertToInMemoryObject(policies)
    return policiesConverted

def getPolicyInceptionsMTAs():
    db = getDB()
    collection = db['policyEvents']
    policies = collection.find({'$or': [{'eventData._t':'WrittenPolicyCreatedEvent'}, {'eventData._t':'PolicyMtaCreatedEvent'}], 'eventData.productType':'CarWrisk', 'eventData.createdDate': {'$gte': start, '$lte': end} })
    print('Processing ' + str(policies.count()) + ' MTAs and Inceptions')
    policiesConverted = convertToInMemoryObject(policies)
    return policiesConverted

def getQuotes():
    db = getDB()
    collection = db['quotes']
    quotes = collection.find({ "$or": [{'status':'UnderWritten'}, {'status':'Completed'}], 'startedDate': {'$gte': start, '$lte': end}})
    print('Processing ' + str(quotes.count()) + ' Quotes')
    quotesConverted = convertToInMemoryObject(quotes)
    return quotesConverted

def getLegalPoliciesIncepted():
    db = getDB()
    collection = db['policyEvents']
    policies = collection.find({'eventData._t':'WrittenPolicyCreatedEvent', 'eventData.productType':'Legal'})
    policiesConverted = convertToInMemoryObject(policies)
    return policiesConverted

def getLegalPoliciesCancelled():
    db = getDB()
    collection = db['policyEvents']
    policies = collection.find({'eventData._t':'PolicyCancelledEvent', 'eventData.productType':'Legal'})
    policiesConverted = convertToInMemoryObject(policies)
    return policiesConverted

def get7DayPolicies():
    db = getDB()
    collection = db['policies']
    policies = collection.find({'productType':'CarWrisk','paymentAuthCode':'FreePolicyNoPaymentTaken'})
    policiesConverted = convertToInMemoryObject(policies)
    return policiesConverted

def getPolicyInceptions():
    db = getDB()
    collection = db['policyEvents']
    policies = collection.find({'eventData._t':'WrittenPolicyCreatedEvent', 'eventData.productType':'CarWrisk', 'eventData.createdDate': {'$gte': start, '$lte': end}})
    print('Processing ' + str(policies.count()) + ' Policy Inceptions')
    policiesConverted = convertToInMemoryObject(policies)
    return policiesConverted

def getPoliciesAmended():
    db = getDB()
    collection = db['policyEvents']
    policies = collection.find({'eventData._t':'PolicyMtaCreatedEvent', 'eventData.productType':'CarWrisk', 'eventData.createdDate': {'$gte': start, '$lte': end}})
    print('Processing ' + str(policies.count()) + ' Policy MTAs')
    policiesConverted = convertToInMemoryObject(policies)
    return policiesConverted

def getPoliciesCancelled():
    db = getDB()
    collection = db['policyEvents']
    policies = collection.find({'eventData._t':'PolicyCancelledEvent', 'eventData.productType':'CarWrisk', 'eventData.displayLevel':'Important', 'eventData.createdDate': {'$gte': start, '$lte': end}})
    print('Processing ' + str(policies.count()) + ' Policy Cancellations')
    policiesConverted = convertToInMemoryObject(policies)
    return policiesConverted

def getLastPolicyState(policy):
    db = getDB()
    collection = db['policies']
    pprint(policy['eventData']['quoteReferenceId'])
    policyStates = collection.find({'policyReferenceId':str(policy['eventData']['quoteReferenceId'])}).sort("createdDate", pymongo.DESCENDING)
    states = []
    for state in policyStates:
        states.append(state)
    return states[0]

def getAgents():
    db = getDB()
    collection = db['users']
    agents = collection.find({'roles.0':'agent'})
    print('Processing ' + str(agents.count()) + ' Agents')
    return agents

def convertToInMemoryObject(mongoCursor):
    newDict = []
    for object in mongoCursor:
        newDict.append(object)
    return newDict
