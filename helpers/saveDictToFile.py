from pprint import pprint
import csv
import boto3
from smart_open import smart_open
from StringIO import StringIO
from datetime import datetime

s3 = boto3.resource('s3')
bucket = s3.Bucket('sp-co-wrisk-359932479723-10-data-import')

start = ""
filetype = "csv"

def setFileDate(day, month, year):
    global start
    start = datetime(year, month, day).isoformat()

def setFileType(type):
    global filetype
    filetype = type


def saveDictToS3(items, key):
    f = StringIO()
    with smart_open('s3://sp-co-wrisk-359932479723-10-data-import/' + key + start +'.csv', 'wb') as fout:
        writer = csv.DictWriter(f, fieldnames=items[0].keys())
        writer.writeheader()
        fout.write(f.getvalue())
        for item in items:
            writer.writerow(item)
            f.seek(0)
            f.truncate(0)
            writer.writerow(item)
            fout.write(f.getvalue())
    f.close()

def saveDictToCSV(items, filepath):
    print('Writing ' + str(len(items)) + ' items to CSV')
    with open(filepath + start + '.csv','wb') as file:
            writer = csv.DictWriter(file, fieldnames=items[0].keys())
            writer.writeheader()
            for item in items:
                writer.writerow(item)

def saveDictToFile(items, filepath):
    if filetype == 'csv':
        saveDictToCSV(items, filepath)
    elif filetype == 's3':
        saveDictToS3(items, filepath)
