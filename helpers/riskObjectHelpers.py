from pprint import pprint
from helpers.hhClient import *

def appendData(quoteOrPolicy, attributes, type):
    if type == 'policy':
        attributes["policy_id"] = quoteOrPolicy['quoteReferenceId']
        attributes["pol_term_no"] = (1)
        attributes["pol_term_seq_no"] = (None)
        attributes["transaction_stored"] = quoteOrPolicy['createdDate']
        attributes["transaction_id"] = quoteOrPolicy['eventId']
    elif type == 'quote':
        attributes["quote_id"] = quoteOrPolicy['quoteReferenceId']
        attributes["transaction_stored"] = quoteOrPolicy['updatedDate']
    attributes["risk_object_id"] = 'Vehicle.1'
    return attributes

def getQuote(id):
    db = getDB()
    collection = db['quotes']
    quote = collection.find_one({ "$or": [{'status':'UnderWritten'}, {'status':'Completed'}], 'productType':'CarWrisk', "quoteReferenceId":id})
    return quote
