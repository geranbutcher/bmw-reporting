from pprint import pprint

def getTotalPremium(doc, type, transaction, doc2={}):
    if type == 'quote':
        base = float(doc['quoteData']['quoteResults'][0]['premium'])
        legal = getLegalCover(doc['quoteData']['options'])
    elif type =='policy':
        if transaction == 'inception':
            base = float(doc['eventData']['quoteData']['prices'][0]['baseSinglePayment'])
            # legal = getLegalCover(doc['eventData']['quoteData']['options'])
            legal = 0
        elif transaction == 'mta':
            base = float(doc['eventData']['mtaAmount'])
            legal = 0
        elif transaction == 'cancellationAnnual':
            base = float(doc['eventData']['additionalCancellationInfo']['policyRefundAmount']) * -1
            legal = getLegalCover(doc2['policyData']['options']) * -1
        elif transaction == 'cancellationMonthly':
            base = float(doc['policyData']['premium']) * -1
            legal = getLegalCover(doc2['policyData']['options']) * -1
    premium = base - legal
    return premium

def getTotalPremiumReconciliation(doc, type, transaction, doc2={}):
    if type == 'quote':
        base = float(doc['quoteData']['quoteResults'][0]['premium'])
    elif type =='policy':
        if transaction == 'inceptionAnnual':
            base = float(doc['eventData']['quoteData']['prices'][0]['singlePayment'])
        elif transaction == 'inceptionMonthly':
            base = float(doc['eventData']['quoteData']['prices'][0]['deposit'])
        elif transaction == 'mta':
            base = float(doc['eventData']['mtaAmount'])
        elif transaction == 'cancellationAnnual':
            base = float(doc['eventData']['additionalCancellationInfo']['policyRefundAmount']) * -1
        elif transaction == 'cancellationMonthly':
            base = float(doc['eventData']['additionalCancellationInfo']['policyRefundAmount']) * -1
    premium = base
    return premium

def calcGrossPremium(totalPremium, ipt):
    premium = totalPremium - ipt
    return premium

def calcNetPremium(grossPremium, commission):
    premium = grossPremium - commission
    return premium

def calcIPT(totalPremium):
    taxRate = 0.12
    ipt = round((totalPremium * taxRate) / (1 + taxRate), 2)
    return ipt

def calcCommission(grossPremium):
    commissionRate = 0.10
    commission = round((grossPremium * commissionRate), 2)
    return commission

def getLegalCover(options):
    x = 0
    for option in options:
        if option['type'] == 'legal' and option['selected'] == True:
            x = float(option['price'])
    return x

def proRataCancellation(policy, state):
    cancellationDate = policy['eventData']['cancellationDate']
    inceptionDate = state['originalPolicyStartDate']
    originalEndDate = state['policyEndDate']
    originalPolicyTime = float((originalEndDate - inceptionDate).total_seconds())
    remainingPolicyTime = float((originalEndDate - cancellationDate).total_seconds())
    fraction = remainingPolicyTime / originalPolicyTime
    proRataPremium = round(getTotalPremium(state, 'policy', 'cancellationMonthly', doc2=state) * fraction, 2)
    return proRataPremium

def getPaymentSchedule(policy, type):
    if type == 'inception':
        if policy['eventData']['quoteData']['prices'][0]['singlePaymentSelected'] == False:
            monthlyYearly = 'MONTHLY'
        elif policy['eventData']['quoteData']['prices'][0]['singlePaymentSelected'] == True:
            monthlyYearly = 'ANNUAL'
    elif type == 'mta':
        if policy['eventData']['policyData']['singlePaymentSelected'] == False:
            monthlyYearly = 'MONTHLY'
        elif policy['eventData']['policyData']['singlePaymentSelected'] == True:
            monthlyYearly = 'ANNUAL'
    elif type == 'cancellation':
        if policy['policyData']['singlePaymentSelected'] == False:
            monthlyYearly = 'MONTHLY'
        elif policy['policyData']['singlePaymentSelected'] == True:
            monthlyYearly = 'ANNUAL'
    return monthlyYearly
