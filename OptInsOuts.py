from additionalExtracts.customerOptIns import *
from helpers.hhClient import *
from multiprocessing import Process
import time

def start():
    start = time.time()
    setStartDate(1,9,2018)
    setEndDate(1,12,2019)
    p1 = Process(target=generateCustomerOptInsOuts)

    p1.start()

    p1.join()

    end = time.time()
    length = str(end - start)
    print("Took " + length + " seconds to run.")

def BMWReportsHandler(event, context):
    start()

if __name__ == "__main__":
    start()

def BMWReportsHandler(event, context):
    start()
