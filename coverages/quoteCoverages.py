from helpers.hhClient import *
from pprint import pprint
from helpers.premiumCalculation import *
from helpers.saveDictToFile import *
import pymongo
import csv

def getNCBProtection(quote):
    try:
        premium = quote['eventData']['quoteData']['quoteResults'][0]['additionalData']['protectedNoClaimsCost']['_v']
    except:
        premium = 0
    return premium

def calcExcess(quote):
    drivers = quote['quoteData']['quoteResults'][0]['excesses']['quoteExcesses'][0]['insuredEntityExcesses']
    excesses = []
    for driver in drivers:
        excess = driver['excesses'][0]['total']
        excesses.append(excess)
    highestexcess = max(excesses)
    return highestexcess

def getMRQuoteCoverages():
    quotes = getQuotes()
    inceptions = []
    for quote in quotes:
        comp = {}
        comp['quote_id'] = quote['quoteReferenceId']
        comp['pol_term_no'] = 1
        comp['pol_term_seq_no'] = None
        comp['transaction_stored'] = str(quote['updatedDate'])
        comp['risk_object_id'] = 'Vehicle.1'
        comp['coverage_type'] = 'COMPREHENSIVE'
        comp['limit'] = quote['modelData']['productData']['carValue']
        comp['excess'] = calcExcess(quote)
        comp['co_payment'] = 0
        totalPremium = getTotalPremium(quote, 'quote', 'quote')
        ipt = calcIPT(totalPremium)
        grossPremium = calcGrossPremium(totalPremium, ipt)
        comp['gross_premium'] = grossPremium
        inceptions.append(comp)
    return inceptions

def getQuoteCoverages(policies):
    rows = []
    i = getMRQuoteCoverages()
    rows.extend(i)
    return rows

def generateQuoteCoveragesExtract():
    quotes = getMRQuoteCoverages()
    saveDictToFile(quotes,'coverages/extracts/quoteCoverages')
