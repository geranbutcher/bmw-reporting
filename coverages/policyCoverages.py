from helpers.hhClient import *
from pprint import pprint
from helpers.premiumCalculation import *
from helpers.saveDictToFile import *
import pymongo
import csv

taxRate = 0.125
commissionRate = 0.10

def getPolicyEventsDB(client):
    db = client['hug-wrisk']
    return db

def getPolicyEventsCollection(db):
    collection = db['policyEvents']
    return collection

def getPoliciesCollection(db):
    collection = db['policies']
    return collection

def getPolicyInceptions(collection):
    policys = collection.find({'eventData._t':'WrittenPolicyCreatedEvent', 'eventData.productType':'CarWrisk'})
    return policys

def getPolicyAmended(collection):
    policys = collection.find({'eventData._t':'PolicyMtaCreatedEvent', 'eventData.productType':'CarWrisk'})
    return policys

def getPolicyCancellations(collection):
    policys = collection.find({'eventData._t':'PolicyCancelledEvent', 'eventData.productType':'CarWrisk', 'eventData.displayLevel':'Important'})
    return policys

def getNCBProtection(policy):
    try:
        premium = policy['eventData']['quoteData']['quoteResults'][0]['additionalData']['protectedNoClaimsCost']['_v']
    except:
        premium = 0
    return premium

def calcExcess(policy, type):
    if type == 'inception':
        drivers = policy['eventData']['excesses']['insuredEntityExcesses']
    elif type == 'cancellation':
        drivers = policy['policyData']['excesses']['insuredEntityExcesses']
    else:
        drivers = policy['eventData']['policyData']['excesses']['insuredEntityExcesses']
    excesses = []
    for driver in drivers:
        excess = driver['excesses'][0]['total']
        excesses.append(excess)
    highestexcess = max(excesses)
    return highestexcess

def proRataCancellation(policy, state):
    cancellationDate = policy['eventData']['cancellationDate']
    inceptionDate = state['originalPolicyStartDate']
    originalEndDate = state['policyEndDate']
    originalPolicyTime = float((originalEndDate - inceptionDate).total_seconds())
    remainingPolicyTime = float((originalEndDate - cancellationDate).total_seconds())
    fraction = remainingPolicyTime / originalPolicyTime
    proRataPremium = round(getTotalPremium(state, 'policy', 'cancellationMonthly', doc2=state) * fraction * -1, 2)
    return proRataPremium

def getPolicyCancelledData(db, id):
    collection = getPoliciesCollection(db)
    policy = list(collection.find({'policyReferenceId': id}).sort("createdDate", pymongo.DESCENDING))[0]
    return policy

def getMRInceptionCoverages(collection):
    policys = getPolicyInceptions(collection)
    inceptions = []
    for policy in policys:
        comp = {}
        if (policy['eventData']['policyEndDate'] - policy['eventData']['policyStartDate']).days < 8:
            totalPremium = 0
            ipt = 0
        else:
            totalPremium = getTotalPremium(policy, 'policy', 'inception')
            ipt = calcIPT(totalPremium)
        grossPremium = calcGrossPremium(totalPremium, ipt)
        commission = calcCommission(grossPremium)
        netPremium = calcNetPremium(grossPremium, commission)
        comp['transaction_id'] = str(policy['_id'])
        comp['policy_id'] = policy['eventData']['quoteReferenceId']
        comp['pol_term_no'] = 1
        comp['pol_term_seq_no'] = None
        comp['transaction_stored'] = str(policy['eventData']['createdDate'])
        comp['risk_object_id'] = 'Vehicle.1'
        comp['coverage_type'] = 'COMPREHENSIVE'
        comp['limit'] = policy['eventData']['modelData']['productData']['carValue']
        comp['excess'] = calcExcess(policy, "inception")
        comp['co_payment'] = 0
        comp['gross_premium'] = grossPremium
        inceptions.append(comp)
    return inceptions

def getMRamendmentCoverages(collection):
    policys = getPolicyAmended(collection)
    amendments = []
    for policy in policys:
        comp = {}

        totalPremium = getTotalPremium(policy, 'policy', 'mta')
        ipt = calcIPT(totalPremium)
        grossPremium = calcGrossPremium(totalPremium, ipt)
        commission = calcCommission(grossPremium)
        netPremium = calcNetPremium(grossPremium, commission)
        comp['transaction_id'] = str(policy['_id'])
        comp['policy_id'] = policy['eventData']['quoteReferenceId']
        comp['pol_term_no'] = 1
        comp['pol_term_seq_no'] = None
        comp['transaction_stored'] = str(policy['eventData']['createdDate'])
        comp['risk_object_id'] = 'Vehicle.1'
        comp['coverage_type'] = 'COMPREHENSIVE'
        comp['limit'] = policy['eventData']['modelData']['productData']['carValue']
        comp['excess'] = calcExcess(policy, "amendment")
        comp['co_payment'] = 0
        comp['gross_premium'] = grossPremium
        amendments.append(comp)
    return amendments

def getMRcancellationCoverages(collection, db):
    cancellations = getPolicyCancellations(collection)
    amendments = []
    for cancellation in cancellations:
        policy = getPolicyCancelledData(db, cancellation['eventData']['quoteReferenceId'])
        comp = {}
        if policy['policyData']['singlePaymentSelected'] == False:
            monthlyYearly = 'MONTHLY'
        elif policy['policyData']['singlePaymentSelected'] == True:
            monthlyYearly = 'ANNUAL'

        if monthlyYearly == 'MONTHLY':
            totalPremium = proRataCancellation(cancellation, policy)
        else:
            totalPremium = getTotalPremium(cancellation, 'policy', 'cancellationAnnual', doc2=policy)
        ipt = calcIPT(totalPremium)
        grossPremium = calcGrossPremium(totalPremium, ipt)
        commission = calcCommission(grossPremium)
        netPremium = calcNetPremium(grossPremium, commission)
        comp['transaction_id'] = str(cancellation['_id'])
        comp['policy_id'] = policy['quoteReferenceId']
        comp['pol_term_no'] = 1
        comp['pol_term_seq_no'] = None
        comp['transaction_stored'] = str(cancellation['eventData']['createdDate'])
        comp['risk_object_id'] = 'Vehicle.1'
        comp['coverage_type'] = 'COMPREHENSIVE'
        comp['limit'] = policy['modelData']['productData']['carValue']
        comp['excess'] = calcExcess(policy, "cancellation")
        comp['co_payment'] = 0
        comp['gross_premium'] = grossPremium
        amendments.append(comp)
    return amendments

def getPolicyCoverages(policies, db):
    rows = []
    i = getMRInceptionCoverages(policies)
    mta = getMRamendmentCoverages(policies)
    c =  getMRcancellationCoverages(policies, db)
    rows.extend(i)
    rows.extend(mta)
    rows.extend(c)
    return rows

def generatePolicyCoveragesExtract():
    c = hhClient()
    db = getPolicyEventsDB(c)
    coll = getPolicyEventsCollection(db)
    policys = getPolicyCoverages(coll, db)
    saveDictToFile(policys, 'coverages/extracts/policyCoverages')
