from reconciliation.hhReconciliationTransactions import *
from reconciliation.stripeChargesAndRefunds import *
from reconciliation.stripePayouts import *
from transactions.policyTransactions import *
from additionalExtracts.legalCover import *

from multiprocessing import Process
import time

def start():
        start = time.time()
        setStartDate(1,9,2018)
        setEndDate(1,12,2019)
        p1 = Process(target=generateStripePayoutsExtract)
        p2 = Process(target=generateHHReconciliationExtract)
        p3 = Process(target=generatePolicyTransactionsExtract)
        p4 = Process(target=generateStripeTransactionsExtract)
        p5 = Process(target=generateLegalCoverExtract)

        p1.start()
        p2.start()
        p3.start()
        p4.start()
        p5.start()

        p1.join()
        p2.join()
        p3.join()
        p4.join()
        p5.join()

        end = time.time()
        length = str(end - start)
        print("Took " + length + " seconds to run.")

def ReconciliationHandler(event, context):
    start()

if __name__ == "__main__":
    start()
