from transactions.policyTransactions import *
from transactions.quoteTransactions import *
from coverages.policyCoverages import *
from coverages.quoteCoverages import *
from riskObjects.policyRiskObject import *
from riskObjects.quoteRiskObject import *
from additionalExtracts.policyholderNames import *
from multiprocessing import Process
import time

def start():
    start = time.time()
    setStartDate(1,3,2019)
    setEndDate(1,12,2019)
    setFileDate(1,3,2019)
    p6 = Process(target=generatePolicyRiskObjectsExtract)
    p7 = Process(target=generateQuoteRiskObjectsExtract)

    p6.start()
    p7.start()

    p6.join()
    p7.join()

    end = time.time()
    length = str(end - start)
    print("Took " + length + " seconds to run.")

def BMWReportsHandler(event, context):
    start()

if __name__ == "__main__":
    start()

def BMWReportsHandler(event, context):
    start()
