from transactions.policyTransactions import *
from transactions.quoteTransactions import *
from coverages.policyCoverages import *
from coverages.quoteCoverages import *
from riskObjects.policyRiskObject import *
from riskObjects.quoteRiskObject import *
from additionalExtracts.policyholderNames import *
from multiprocessing import Process
import time

def start():
    start = time.time()
    setStartDate(1,3,2019)
    setEndDate(1,12,2019)
    setFileDate(1,3,2019)
    p1 = Process(target=generatePolicyTransactionsExtract)
    p2 = Process(target=generatePolicyHolderNamesExtract)
    p3 = Process(target=generateQuoteTransactionsExtract)
    p4 = Process(target=generatePolicyCoveragesExtract)
    p5 = Process(target=generateQuoteCoveragesExtract)
    p6 = Process(target=generatePolicyRiskObjectsExtract)
    p7 = Process(target=generateQuoteRiskObjectsExtract)

    p1.start()
    p2.start()
    p3.start()
    p4.start()
    p5.start()
    p6.start()
    p7.start()

    p1.join()
    p2.join()
    p3.join()
    p4.join()
    p5.join()
    p6.join()
    p7.join()

    end = time.time()
    length = str(end - start)
    print("Took " + length + " seconds to run.")

def BMWReportsHandler(event, context):
    start()

if __name__ == "__main__":
    start()

def BMWReportsHandler(event, context):
    start()
