from transactions.policyTransactions import *
from transactions.quoteTransactions import *
from additionalExtracts.policyholderNames import *
from additionalExtracts.policyVehicleDetails import *
from additionalExtracts.agentAttribution import *
from additionalExtracts.retailerAttributionPolicies import *
from additionalExtracts.retailerAttributionQuotes import *
from additionalExtracts.legalCover import *
from additionalExtracts.quoteVehicleDetails import *
from additionalExtracts.funnel import *
from helpers.hhClient import *
from multiprocessing import Process
import time

def start():
    start = time.time()
    setStartDate(1,9,2018)
    setEndDate(1,12,2019)
    p1 = Process(target=generatePolicyTransactionsExtract)
    p2 = Process(target=generatePolicyHolderNamesExtract)
    p3 = Process(target=generateQuoteTransactionsExtract)
    p4 = Process(target=generatePolicyVehicleDetailsExtract)
    p5 = Process(target=generateAgentExtracts)
    p6 = Process(target=generateRetailerAttributionPolicyExtract)
    p7 = Process(target=generateLegalCoverExtract)
    p8 = Process(target=generateQuoteVehicleDetails)
    p9 = Process(target=generateFunnel)
    p10 = Process(target=generateRetailerAttributionQuoteExtract)

    p1.start()
    p2.start()
    p3.start()
    p4.start()
    p5.start()
    p6.start()
    p7.start()
    p8.start()
    p9.start()
    p10.start()


    p1.join()
    p2.join()
    p3.join()
    p4.join()
    p5.join()
    p6.join()
    p7.join()
    p8.join()
    p9.join()
    p10.join()
    end = time.time()
    length = str(end - start)
    print("Took " + length + " seconds to run.")

def BMWReportsHandler(event, context):
    start()

if __name__ == "__main__":
    start()

def BMWReportsHandler(event, context):
    start()
