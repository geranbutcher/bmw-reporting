from helpers.riskObjectHelpers import *
from pprint import pprint
from datetime import datetime
from math import floor

def relationship(driver, doc, number):
    attributes = {}
    attributes['value'] = driver['relationshipToPolicyholder']['description']
    attributes['name'] = 'AdditionalDriver.' + str(number) + '.RelationshipToPolicyHolder'
    attributes = appendData(doc, attributes, doc['type'])
    return attributes

def drivingLicenceType(driver, doc, number):
    attributes = {}
    attributes['value'] = driver['drivingLicence']['description']
    attributes['name'] = 'AdditionalDriver.' + str(number) + '.DrivingLicenceType'
    attributes = appendData(doc, attributes, doc['type'])
    return attributes

def drivingLicenceYear(driver, doc, number):
    attributes = {}
    attributes['value'] = driver['drivingLicenceYear']['description']
    attributes['name'] = 'AdditionalDriver.' + str(number) + '.DrivingLicenceYear'
    attributes = appendData(doc, attributes, doc['type'])
    return attributes

def dateofBirth(driver, doc, number):
    attributes = {}
    attributes['value'] = floor((datetime.utcnow() - driver['dateOfBirth']).days/float(365))
    attributes['name'] = 'AdditionalDriver.' + str(number) + '.Age'
    attributes = appendData(doc, attributes, doc['type'])
    return attributes

def industry(driver, doc, number):
    attributes = {}
    try:
        attributes['value'] = driver['businessType']['description']
    except:
        attributes['value'] = None
    attributes['name'] = 'AdditionalDriver.' + str(number) + '.Industry'
    attributes = appendData(doc, attributes, doc['type'])
    return attributes

def employmentType(driver, doc, number):
    attributes = {}
    attributes['value'] = driver['employmentType']['description']
    attributes['name'] = 'AdditionalDriver.' + str(number) + '.EmploymentStatus'
    attributes = appendData(doc, attributes, doc['type'])
    return attributes

def occupation(driver, doc, number):
    attributes = {}
    try:
        attributes['value'] = driver['occupation']['description']
    except:
        attributes['value'] = None
    attributes['name'] = 'AdditionalDriver.' + str(number) + '.Occupation'
    attributes = appendData(doc, attributes, doc['type'])
    return attributes

def residency(driver, doc, number):
    attributes = {}
    attributes['value'] = driver['residency']['description']
    attributes['name'] = 'AdditionalDriver.' + str(number) + '.Residency'
    attributes = appendData(doc, attributes, doc['type'])
    return attributes

def claims(driver, doc, number):
    attributeslist = []
    claimsPresent = len(driver['namedDriversDetails']['claimDetails'])
    attributes = {}
    attributes['value'] = claimsPresent
    attributes['name'] = 'AdditionalDriver.' + str(number) + '.ClaimCount'
    attributes = appendData(doc, attributes, doc['type'])
    attributeslist.append(attributes)
    if claimsPresent > 0:
        claims = driver['namedDriversDetails']['claimDetails']
        x = 1
        for claim in claims:
            claimFields = [
                            # {'field': 'Fault', 'locator': claim['ndclaimsAtFault']['DM_ENUMERATION']},
                            # {'field': 'ClaimNCBLost', 'locator': claim['ndclaimsNCBLost']['DM_ENUMERATION']},
                            {'field': 'ClaimSettled', 'locator': claim['ndclaimsSettledYN']},
                            {'field': 'dateOfIncident', 'locator': claim['nddateOfIncident']},
                            {'field': 'IncidentType', 'locator': claim['ndtypeOfIncident']['description']}
                           ]
            claimAttribute = {}
            for field in claimFields:
                claimAttribute['value'] = field['locator']
                claimAttribute['name'] = 'AdditionalDriver.' + str(number) + '.Claim.' + str(x) + '.' + field['field']
                claimAttribute = appendData(doc, claimAttribute, doc['type'])
                attributeslist.append(claimAttribute)
            x = x + 1
    return attributeslist

def convictions(driver, doc, number):
    attributeslist = []
    convictionsPresent = len(driver['namedDriversDetails']['convictionDetails'])
    attributes = {}
    attributes['value'] = convictionsPresent
    attributes['name'] = 'AdditionalDriver.' + str(number) + '.ConvictionCount'
    attributes = appendData(doc, attributes, doc['type'])
    attributeslist.append(attributes)
    if convictionsPresent > 0:
        convictions = driver['namedDriversDetails']['convictionDetails']
        x = 1
        for conviction in convictions:
            convictionFields = [
                            {'field': 'Points', 'locator': conviction['ndconvictionsPoints']},
                            {'field': 'DateOfBirth', 'locator': conviction['nddateOfOffense']},
                            {'field': 'Type', 'locator': conviction['ndtypeOfOffense']['description']}
                           ]
            convictionAttribute = {}
            for field in convictionFields:
                convictionAttribute['value'] = field['locator']
                convictionAttribute['name'] = 'AdditionalDriver.' + str(number) + 'Conviction.' + str(x) + '.' + field['field']
                convictionAttribute = appendData(doc, convictionAttribute, doc['type'])
                attributeslist.append(convictionAttribute)
            x = x + 1
    return attributeslist


def addNamedDrivers(doc):
    fullDriversList = []
    if doc['type'] == 'policy':
        drivers = doc['eventData']['modelData']['productData']['namedDrivers']
    elif doc['type'] == 'quote':
        drivers = doc['modelData']['productData']['namedDrivers']
    fullDriversList = []
    number = 1
    for driver in drivers:
        driverAttributes = []
        driverAttributes.append(drivingLicenceType(driver, doc, number))
        driverAttributes.append(drivingLicenceYear(driver, doc, number))
        driverAttributes.append(dateofBirth(driver, doc, number))
        driverAttributes.append(residency(driver, doc, number))
        driverAttributes.append(employmentType(driver, doc, number))
        driverAttributes.append(industry(driver, doc, number))
        driverAttributes.append(occupation(driver, doc, number))
        driverAttributes.extend(claims(driver, doc, number))
        driverAttributes.extend(convictions(driver, doc, number))
        fullDriversList.extend(driverAttributes)
        number = number + 1
    return fullDriversList
