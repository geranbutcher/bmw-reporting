from helpers.riskObjectHelpers import *
from pprint import pprint
from datetime import datetime
from math import floor

def postcode(doc):
    attributes = {}
    attributes['value'] = doc['modelData']['personalDetails']['address']['postcode']
    attributes['name'] = 'MainDriver.PostCode'
    attributes = appendData(doc, attributes, doc['type'])
    return attributes

def advisedToDrive(doc):
    attributes = {}
    attributes['value'] = doc['modelData']['personalDetails']['additionalPersonalDetails']['advisedNotToDriveYN']
    attributes['name'] = 'MainDriver.AdvisedNotToDrive'
    attributes = appendData(doc, attributes, doc['type'])
    return attributes

def drivingLicenceType(doc):
    attributes = {}
    attributes['value'] = doc['modelData']['personalDetails']['additionalPersonalDetails']['drivingLicence']['description']
    attributes['name'] = 'MainDriver.DrivingLicenceType'
    attributes = appendData(doc, attributes, doc['type'])
    return attributes

def drivingLicenceYears(doc):
    attributes = {}
    attributes['value'] = doc['modelData']['personalDetails']['additionalPersonalDetails']['drivingLicenceYear']['description']
    attributes['name'] = 'MainDriver.DrivingLicenceYears'
    attributes = appendData(doc, attributes, doc['type'])
    return attributes

def employmentStatus(doc):
    attributes = {}
    attributes['value'] = doc['modelData']['personalDetails']['employmentType']['description']
    attributes['name'] = 'MainDriver.EmploymentStatus'
    attributes = appendData(doc, attributes, doc['type'])
    return attributes

def medicalConditions(doc):
    attributes = {}
    attributes['value'] = doc['modelData']['personalDetails']['additionalPersonalDetails']['medicalConditionsYN']
    attributes['name'] = 'MainDriver.MedicalConditionsYN'
    attributes = appendData(doc, attributes, doc['type'])
    return attributes

def daysToInception():
    attributes = {}
    if doc['type'] == 'quote':
        attributes['value'] = (doc['modelData']['desiredStartDate'] - doc['updatedDate']).days
    attributes['name'] = 'DaysToInception'
    attributes = appendData(doc, attributes, doc['type'])

def industry(doc):
    attributes = {}
    try:
        attributes['value'] = doc['modelData']['personalDetails']['businessType']['description']
    except:
        attributes['value'] = None
    attributes['name'] = 'MainDriver.OccupationIndustry'
    attributes = appendData(doc, attributes, doc['type'])
    return attributes

def registeredKeeper(doc):
    attributes = {}
    try:
        attributes['value'] = doc['modelData']['productData']['additionalCarDetails']['registeredKeeperYesNo']
    except:
        attributes['value'] = None
    attributes['name'] = 'MainDriver.RegisteredKeeper'
    attributes = appendData(doc, attributes, doc['type'])
    return attributes

def NCDprotected(doc):
    attributes = {}
    try:
        attributes['value'] = doc['modelData']['productData']['additionalPolicyDetails']['policyNoClaimsBonusProtected']
    except:
        attributes['value'] = None
    attributes['name'] = 'MainDriver.NCDprotectedYN'
    attributes = appendData(doc, attributes, doc['type'])
    return attributes

def legalOwner(doc):
    attributes = {}
    try:
        attributes['value'] = doc['modelData']['productData']['additionalCarDetails']['legalOwnerYN']
    except:
        attributes['value'] = None
    attributes['name'] = 'MainDriver.LegalOwner'
    attributes = appendData(doc, attributes, doc['type'])
    return attributes

def occupation(doc):
    attributes = {}
    try:
        attributes['value'] = doc['modelData']['personalDetails']['occupation']['description']
    except:
        attributes['value'] = None
    attributes['name'] = 'MainDriver.Occupation'
    attributes = appendData(doc, attributes, doc['type'])
    return attributes

def dateOfBirth(doc):
    attributes = {}
    attributes['value'] = floor((datetime.utcnow() - doc['modelData']['personalDetails']['dateOfBirth']).days/float(365))
    attributes['name'] = 'MainDriver.Age'
    attributes = appendData(doc, attributes, doc['type'])
    return attributes

def maritalStatus(doc):
    attributes = {}
    attributes['value'] = doc['modelData']['personalDetails']['maritalStatus']['description']
    attributes['name'] = 'MainDriver.MaritalStatus'
    attributes = appendData(doc, attributes, doc['type'])
    return attributes

def vehicleLengthDriven(doc):
    attributes = {}
    try:
        attributes['value'] = datetime.utcnow() - doc['modelData']['productData']['purchaseDate'].days/float(365) / 12
    except:
        attributes['value'] = 0
    attributes['name'] = 'MainDriver.VehicleLengthDriven'
    attributes = appendData(doc, attributes, doc['type'])
    return attributes

def residency(doc):
    attributes = {}
    try:
        attributes['value'] = doc['modelData']['personalDetails']['residency']['description']
    except:
        attributes['value'] = None
    attributes['name'] = 'MainDriver.UKResidency'
    attributes = appendData(doc, attributes, doc['type'])
    return attributes

def voluntaryExcess(doc):
    quote = getQuote(doc['quoteReferenceId'])
    attributes = {}
    try:
        excesses = quote['quoteData']['quoteResults'][0]['excesses']['quoteExcesses'][1]['insuredEntityExcesses'][0]['excesses']
        for excess in excesses:
            if excess['name'] == 'Accidental Damage':
                attributes['value'] = excess['components'][1]['amount']
    except:
        attributes['value'] = None
    attributes['name'] = 'MainDriver.VoluntaryExcess'
    attributes = appendData(doc, attributes, doc['type'])
    return attributes

def propertyOwnership(doc):
    attributes = {}
    try:
        attributes['value']  = doc['modelData']['personalDetails']['propertyOwnership']
    except:
        attributes['value'] = None
    attributes['name'] = 'MainDriver.PropertyOwnership'
    attributes = appendData(doc, attributes, doc['type'])
    return attributes

def vehicleAccess(doc):
    attributes = {}
    attributes['value'] = doc['modelData']['productData']['otherVehiclesAccessYN']
    attributes['name'] = 'MainDriver.OtherVehicleAccess'
    attributes = appendData(doc, attributes, doc['type'])
    return attributes

def claims(doc):
    attributeslist = []
    try:
        claimsPresent = len(doc['modelData']['productData']['claimDetails'])
        attributes = {}
        attributes['value'] = claimsPresent
        attributes['name'] = 'MainDriver.ClaimCount'
        attributes = appendData(doc, attributes, doc['type'])
        attributeslist.append(attributes)
        if claimsPresent > 0:
            claims = doc['modelData']['productData']['claimDetails']
            x = 1
            for claim in claims:
                claimFields = [
                                {'field': 'Fault', 'locator': claim['claimsAtFault']['dM_ENUMERATION']},
                                {'field': 'ClaimNCBLost', 'locator': claim['claimsNCBLost']['dM_ENUMERATION']},
                                {'field': 'ClaimSettled', 'locator': claim['claimsSettledYN']},
                                {'field': 'dateOfIncident', 'locator': claim['dateOfIncident']},
                                {'field': 'IncidentType', 'locator': claim['typeOfIncident']['description']}
                               ]
                claimAttribute = {}
                for field in claimFields:
                    claimAttribute['value'] = field['locator']
                    claimAttribute['name'] = 'MainDriver.Claim.' + str(x) + '.' + field['field']
                    claimAttribute = appendData(doc, claimAttribute, doc['type'])
                    attributeslist.append(claimAttribute)
                x = x + 1
    except:
        pass
    return attributeslist

def convictions(doc):
    attributeslist = []
    convictionsPresent = len(doc['modelData']['productData']['convictionDetails'])
    attributes = {}
    attributes['value'] = convictionsPresent
    attributes['name'] = 'MainDriver.ConvictionCount'
    attributes = appendData(doc, attributes, doc['type'])
    attributeslist.append(attributes)
    if convictionsPresent > 0:
        convictions = doc['modelData']['productData']['convictionDetails']
        x = 1
        for conviction in convictions:
            convictionFields = [
                            {'field': 'Points', 'locator': conviction['convictionsPoints']},
                            {'field': 'DateOfBirth', 'locator': conviction['dateOfOffense']},
                            {'field': 'Type', 'locator': conviction['typeOfOffense']['description']}
                           ]
            convictionAttribute = {}
            for field in convictionFields:
                convictionAttribute['value'] = field['locator']
                convictionAttribute['name'] = 'MainDriver.Conviction.' + str(x) + '.' + field['field']
                convictionAttribute = appendData(doc, convictionAttribute, doc['type'])
                attributeslist.append(convictionAttribute)
            x = x + 1
    return attributeslist
