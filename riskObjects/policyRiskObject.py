from helpers.hhClient import *
from pprint import pprint
import pymongo
import mainDriverRiskObjectAttributes as mdatt
import vehicleRiskObjectAttributes as vatt
from helpers.riskObjectHelpers import *
from additionalDriverRiskObjectAttributes import addNamedDrivers
from helpers.saveDictToFile import *
import csv

def numberOfAdditionalDrivers(policy):
    try:
        n = len(policy['eventData']['modelData']['productData']['namedDrivers'])
    except:
        n = 0
    return n

def getLatestQuote(id):
    db = getDB()
    coll = db['quotes']
    quote = coll.find_one({'productType':'CarWrisk', 'quoteReferenceId':id })
    return quote

def daysToInception(policy):
    id = str(policy['quoteReferenceId'])
    doc = getLatestQuote(id)
    try:
        attributes = {}
        attributes['value'] = round((doc['modelData']['desiredStartDate'] - doc['startedDate']).total_seconds()/60/60/24)
        attributes['name'] = 'DaysToInception'
        attributes = appendData(policy, attributes, policy['type'])
    except:
        pprint(doc)
        print(id)
    return attributes

def createRiskObjectforPolicys(policys):
    policyRiskObject = []
    n = 1
    for policy in policys:
        if (n % 100) == 0:
            print("Processed " + str(n) + " of " + str(len(policys)) + " policies")
        id = str(policy['_id'])
        policy = policy['eventData']
        policy['eventId'] = id
        policyRiskAttributes = []
        policy['type'] = 'policy'

        # Main Driver Attributes
        policyRiskObject.append(mdatt.postcode(policy))
        policyRiskObject.append(mdatt.advisedToDrive(policy))
        policyRiskObject.append(mdatt.drivingLicenceType(policy))
        policyRiskObject.append(mdatt.drivingLicenceYears(policy))
        policyRiskObject.append(mdatt.employmentStatus(policy))
        policyRiskObject.append(mdatt.industry(policy))
        policyRiskObject.append(mdatt.occupation(policy))
        policyRiskObject.append(mdatt.dateOfBirth(policy))
        policyRiskObject.append(mdatt.maritalStatus(policy))
        policyRiskObject.append(mdatt.residency(policy))
        policyRiskObject.append(mdatt.registeredKeeper(policy))
        policyRiskObject.append(mdatt.legalOwner(policy))
        policyRiskObject.append(mdatt.medicalConditions(policy))
        policyRiskObject.append(mdatt.NCDprotected(policy))
        policyRiskObject.append(mdatt.propertyOwnership(policy))
        policyRiskObject.append(mdatt.voluntaryExcess(policy))
        policyRiskObject.append(mdatt.vehicleLengthDriven(policy))
        policyRiskObject.append(daysToInception(policy))

        policyRiskObject.append(mdatt.vehicleAccess(policy))

        # Things that loop need to be extends not appends
        policyRiskObject.extend(mdatt.claims(policy))
        policyRiskObject.extend(mdatt.convictions(policy))
        policyRiskObject.extend(vatt.modifications(policy))

        # Vehicle Attributes
        policyRiskObject.append(vatt.model(policy))
        policyRiskObject.append(vatt.group(policy))
        policyRiskObject.append(vatt.vehicleAge(policy))
        policyRiskObject.append(vatt.vehicleLRDrive(policy))
        policyRiskObject.append(vatt.usage(policy))
        policyRiskObject.append(vatt.mileage(policy))
        policyRiskObject.append(vatt.value(policy))
        policyRiskObject.append(vatt.numberOfDrivers(policy))
        policyRiskObject.append(vatt.ncb(policy))
        policyRiskObject.append(vatt.parking(policy))
        policyRiskObject.append(vatt.tracker(policy))
        policyRiskObject.append(vatt.transmission(policy))
        policyRiskObject.append(vatt.purchaseDate(policy))
        policyRiskObject.append(vatt.brand(policy))
        policyRiskObject.append(vatt.fuelType(policy))

        # Named Driver Attributes
        if numberOfAdditionalDrivers(policy) > 0:
            policyRiskObject.extend(addNamedDrivers(policy))
        n = n + 1
    return policyRiskObject

def writePROToCSV(riskObjects):
    n = str(len(riskObjects))
    print("Writing " + n + " Policy Risk Objects")
    saveDictToFile(riskObjects,'riskObjects/extracts/policyRiskObjects')

def generatePolicyRiskObjectsExtract():
    qs = getPolicyInceptionsMTAs()
    ro = createRiskObjectforPolicys(qs)
    writePROToCSV(ro)
