from helpers.riskObjectHelpers import *
from datetime import datetime
from pprint import pprint

def model(doc):
    attributes = {}
    try:
        attributes['value'] = doc['modelData']['productData']['additionalCarDetails']['vehicleManufacturer']['description'] + ' ' + doc['modelData']['productData']['additionalCarDetails']['vehicleModel']['description']
    except:
        attributes['value'] = doc['modelData']['productData']['additionalCarDetails']['vehicleLookupInfo']['displayName']
    attributes['name'] = 'Vehicle.Model'
    attributes = appendData(doc, attributes, doc['type'])
    return attributes

def transmission(doc):
    attributes = {}
    try:
        attributes['value'] = doc['modelData']['productData']['additionalCarDetails']['vehicleLookupInfo']['transmission']
    except:
        attributes['value'] = None
    attributes['name'] = 'Vehicle.Transmission'
    attributes = appendData(doc, attributes, doc['type'])
    return attributes

def fuelType(doc):
    attributes = {}
    try:
        attributes['value'] = doc['modelData']['productData']['additionalCarDetails']['vehicleLookupInfo']['fuelType']
    except:
        attributes['value'] = None
    attributes['name'] = 'Vehicle.FuelType'
    attributes = appendData(doc, attributes, doc['type'])
    return attributes

def vehicleAge(doc):
    attributes = {}
    try:
        attributes['value'] = int(datetime.utcnow().year) - doc['modelData']['productData']['additionalCarDetails']['vehicleManufacturedYear']
    except:
        attributes['value'] = None
    attributes['name'] = 'Vehicle.VehicleAge'
    attributes = appendData(doc, attributes, doc['type'])
    return attributes

def vehicleAge(doc):
    attributes = {}
    try:
        attributes['value'] = int(datetime.utcnow().year) - doc['modelData']['productData']['additionalCarDetails']['vehicleManufacturedYear']
    except:
        attributes['value'] = None
    attributes['name'] = 'Vehicle.VehicleAge'
    attributes = appendData(doc, attributes, doc['type'])
    return attributes

def brand(doc):
    attributes = {}
    try:
        attributes['value'] = doc['modelData']['productData']['additionalCarDetails']['vehicleManufacturer']['description']
    except:
        attributes['value'] = None
    attributes['name'] = 'Vehicle.Brand'
    attributes = appendData(doc, attributes, doc['type'])
    return attributes

def purchaseDate(doc):
    attributes = {}
    try:
        attributes['value'] =doc['modelData']['productData']['purchaseDate']
    except:
        attributes['value'] = None
    attributes['name'] = 'Vehicle.PurchaseDate'
    attributes = appendData(doc, attributes, doc['type'])
    return attributes


def modifications(doc):
    try:
        attributeslist = []
        numberModsPresent = len(doc['modelData']['productData']['carModificationDetails'])
        if numberModsPresent > 0 and modPresent != None:
            mods = doc['modelData']['productData']['carModificationDetails']
            x = 1
            for mod in mods:
                modAttribute = {}
                modAttribute['value'] = mod['carModificationType']['description']
                modAttribute['name'] = 'Vehicle.Modification.' + str(x)
                modAttribute = appendData(doc, modAttribute, doc['type'])
                attributeslist.append(modAttribute)
                x = x + 1
    except:
        attributeslist = []
    return attributeslist


def totalVehicles(doc):
    attributes = {}
    attributes['value'] = doc['modelData']['productData']['additionalCarDetails']['totalVehiclesInHouseHold']
    attributes['name'] = 'Vehicle.TotalVehicles'
    attributes = appendData(doc, attributes, doc['type'])
    return attributes

def numberOfDrivers(doc):
    attributes = {}
    try:
        attributes['value'] = len(doc['modelData']['productData']['namedDrivers']) + 1
    except:
        attributes['value'] = 1
    attributes['name'] = 'Vehicle.NumberOfNamedDrivers'
    attributes = appendData(doc, attributes, doc['type'])
    return attributes

def vehicleLRDrive(doc):
    attributes = {}
    attributes['value'] = doc['modelData']['productData']['additionalCarDetails']['whichHandDrive']['description']
    attributes['name'] = 'Vehicle.LeftRightDrive'
    attributes = appendData(doc, attributes, doc['type'])
    return attributes

def usage(doc):
    attributes = {}
    attributes['value'] = doc['modelData']['productData']['additionalPolicyDetails']['policyDriverUsage']['description']
    attributes['name'] = 'Vehicle.Usage'
    attributes = appendData(doc, attributes, doc['type'])
    return attributes

def tracker(doc):
    attributes = {}
    attributes['value'] = doc['modelData']['productData']['additionalCarDetails']['vehicleTracker']
    attributes['name'] = 'Vehicle.Tracker'
    attributes = appendData(doc, attributes, doc['type'])
    return attributes

def mileage(doc):
    attributes = {}
    attributes['value'] = doc['modelData']['productData']['milesPerYear']
    attributes['name'] = 'Vehicle.Mileage'
    attributes = appendData(doc, attributes, doc['type'])
    return attributes

def value(doc):
    attributes = {}
    attributes['value'] = doc['modelData']['productData']['carValue']
    attributes['name'] = 'Vehicle.Value'
    attributes = appendData(doc, attributes, doc['type'])
    return attributes

def group(doc):
    attributes = {}
    try:
        attributes['value'] = doc['modelData']['productData']['additionalCarDetails']['vehicleLookupInfo']['code']
    except:
        attributes['value'] = None
    attributes['name'] = 'Vehicle.VehicleGroup'
    attributes = appendData(doc, attributes, doc['type'])
    return attributes

def ncb(doc):
    attributes = {}
    attributes['value'] = doc['modelData']['productData']['additionalPolicyDetails']['policyNoClaimsBonus']['description']
    attributes['name'] = 'Vehicle.NCBYears'
    attributes = appendData(doc, attributes, doc['type'])
    return attributes

def parking(doc):
    attributes = {}
    attributes['value'] = doc['modelData']['productData']['overnightParking']['description']
    attributes['name'] = 'Vehicle.OverNightParkingLocation'
    attributes = appendData(doc, attributes, doc['type'])
    return attributes
