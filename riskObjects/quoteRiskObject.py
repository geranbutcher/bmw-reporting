from pprint import pprint
from helpers.hhClient import *
import pymongo
import mainDriverRiskObjectAttributes as mdatt
import vehicleRiskObjectAttributes as vatt
from additionalDriverRiskObjectAttributes import addNamedDrivers
from helpers.riskObjectHelpers import *
from helpers.saveDictToFile import *
import csv

def numberOfAdditionalDrivers(doc):
    if doc['type'] == 'quote':
        try:
            n = len(doc['modelData']['productData']['namedDrivers'])
        except:
            n = 0
    else:
        n = 0
    return n

def daysToInception(doc):
    id = doc['quoteReferenceId']
    attributes = {}
    attributes['value'] = round((doc['modelData']['desiredStartDate'] - doc['startedDate']).total_seconds()/60/60/24)
    attributes['name'] = 'DaysToInception'
    attributes = appendData(doc, attributes, doc['type'])
    return attributes

def createRiskObjectforQuotes(quotes):
    quoteRiskObject = []
    n = 1
    for quote in quotes:
        if (n % 100) == 0:
            print("Processed " + str(n) + " of " + str(len(quotes)) + " Quotes")
        quoteRiskAttributes = []
        quote['type'] = 'quote'

        # Main Driver Attributes
        quoteRiskObject.append(mdatt.postcode(quote))
        quoteRiskObject.append(mdatt.advisedToDrive(quote))
        quoteRiskObject.append(mdatt.drivingLicenceType(quote))
        quoteRiskObject.append(mdatt.drivingLicenceYears(quote))
        quoteRiskObject.append(mdatt.employmentStatus(quote))
        quoteRiskObject.append(mdatt.industry(quote))
        quoteRiskObject.append(mdatt.occupation(quote))
        quoteRiskObject.append(mdatt.dateOfBirth(quote))
        quoteRiskObject.append(mdatt.maritalStatus(quote))
        quoteRiskObject.append(mdatt.residency(quote))
        quoteRiskObject.append(mdatt.registeredKeeper(quote))
        quoteRiskObject.append(mdatt.legalOwner(quote))
        quoteRiskObject.append(mdatt.propertyOwnership(quote))
        quoteRiskObject.append(mdatt.NCDprotected(quote))
        quoteRiskObject.append(mdatt.medicalConditions(quote))
        quoteRiskObject.append(mdatt.vehicleLengthDriven(quote))
        quoteRiskObject.append(mdatt.voluntaryExcess(quote))
        quoteRiskObject.extend(mdatt.claims(quote))
        quoteRiskObject.append(mdatt.vehicleAccess(quote))
        quoteRiskObject.extend(mdatt.convictions(quote))
        quoteRiskObject.extend(mdatt.convictions(quote))
        quoteRiskObject.append(daysToInception(quote))
        #
        # Vehicle Attributes
        quoteRiskObject.append(vatt.model(quote))
        quoteRiskObject.append(vatt.group(quote))
        quoteRiskObject.append(vatt.vehicleLRDrive(quote))
        quoteRiskObject.append(vatt.vehicleAge(quote))
        quoteRiskObject.extend(vatt.modifications(quote))
        quoteRiskObject.append(vatt.numberOfDrivers(quote))
        quoteRiskObject.append(vatt.usage(quote))
        quoteRiskObject.append(vatt.mileage(quote))
        quoteRiskObject.append(vatt.value(quote))
        quoteRiskObject.append(vatt.ncb(quote))
        quoteRiskObject.append(vatt.parking(quote))
        quoteRiskObject.append(vatt.tracker(quote))
        quoteRiskObject.append(vatt.transmission(quote))
        quoteRiskObject.append(vatt.purchaseDate(quote))
        quoteRiskObject.append(vatt.brand(quote))
        quoteRiskObject.append(vatt.fuelType(quote))

        # Named Driver Attributes
        if numberOfAdditionalDrivers(quote) > 0:
            quoteRiskObject.extend(addNamedDrivers(quote))
        n = n + 1
    return quoteRiskObject

def writeQROToCSV(riskObjects):
    n = str(len(riskObjects))
    print("Writing " + n + " Quote Risk Objects")
    saveDictToFile(riskObjects,'riskObjects/extracts/quoteRiskObjects')


def generateQuoteRiskObjectsExtract():
    qs = getQuotes()
    ro = createRiskObjectforQuotes(qs)
    writeQROToCSV(ro)
