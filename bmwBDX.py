from transactions.policyTransactions import *
from additionalExtracts.policyholderNames import *
from multiprocessing import Process
import time

def start():
    start = time.time()
    setStartDate(1,3,2019)
    setEndDate(1,12,2019)
    p1 = Process(target=generatePolicyTransactionsExtract)
    p2 = Process(target=generatePolicyHolderNamesExtract)

    p1.start()
    p2.start()

    p1.join()
    p2.join()

    end = time.time()
    length = str(end - start)
    print("Took " + length + " seconds to run.")

def BMWReportsHandler(event, context):
    start()

if __name__ == "__main__":
    start()

def BMWReportsHandler(event, context):
    start()
