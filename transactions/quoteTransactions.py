from helpers.hhClient import *
from pprint import pprint
from datetime import timedelta
from helpers.premiumCalculation import *
from helpers.saveDictToFile import *
import pymongo
import csv


def getPolicyID(quote):
    if quote['status'] == 'UnderWritten':
        return quote['quoteReferenceId']
    else:
        return None

def isConverted(quote):
    status = quote['status']
    if status == 'Completed':
        return 'QUOTED'
    elif status == 'UnderWritten':
        return 'BOUND'

def quoteDetails():
    quotes = getQuotes()
    allquoteInceptedDetails = []
    for quote in quotes:
        eventDetails = {}
        totalPremium = getTotalPremium(quote, 'quote', 'quote')
        ipt = calcIPT(totalPremium)
        grossPremium = calcGrossPremium(totalPremium, ipt)
        commission = calcCommission(grossPremium)
        netPremium = calcNetPremium(grossPremium, commission)
        eventDetails['quote_id'] = quote['quoteReferenceId']
        eventDetails['partner_name'] = 'Wrisk'
        eventDetails['product_id'] = 'WriskMiniBMW'
        eventDetails['product_name'] = 'BMW 1.0'
        eventDetails['policy_id'] = getPolicyID(quote)
        eventDetails['transaction_stored'] = str(quote['updatedDate'])
        eventDetails['policy_inception'] = str(quote['modelData']['desiredStartDate'])
        eventDetails['policy_expiry'] = str(quote['modelData']['desiredStartDate'] + timedelta(days=364))
        eventDetails['currency'] = 'GBP'
        eventDetails['total_premium'] = totalPremium
        eventDetails['gross_premium'] = grossPremium
        eventDetails['net_premium'] = netPremium
        eventDetails['total_taxes'] = ipt
        eventDetails['total_commissions'] = commission
        eventDetails['payment_frequency'] = None
        eventDetails['class_of_business'] = 'Motor'
        eventDetails['status'] = isConverted(quote)
        allquoteInceptedDetails.append(eventDetails)
    return allquoteInceptedDetails

def writeQuoteOrigin():
    t = []
    quotes = getQuotes()
    for quote in quotes:
        tq = {}
        tq['quote_id'] = quote['quoteReferenceId']
        tq['creator_id'] = quote['createdById']
        tq['referrer'] = quote['modelData']['personalDetails']['additionalPersonalDetails']['referrer']['description']
        if quote['createdById'] == quote['actingUserId']:
            tq['origin'] = 'Web Self Service'
        elif quote['createdById'] == None:
            tq['origin'] = '7 Day to Annual Conversion'
        else:
            tq['origin'] = 'Wrisk Agent'
        t.append(tq)
    saveDictToFile(t,'transactions/extracts/quoteOrigins.csv')

def generateQuoteTransactionsExtract():
    transactions = quoteDetails()
    saveDictToFile(transactions,'transactions/extracts/quotetransactions')
    writeQuoteOrigin()
    print('Quote Transactions Extract Complete')
