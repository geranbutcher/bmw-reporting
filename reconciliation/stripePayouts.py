import stripe
from helpers.saveDictToFile import *
from pprint import pprint
from datetime import datetime

def getPayouts():
    stripe.api_key = 'sk_live_bDhfHal81HpiY8D19OrkaJ4A'

    payout1 = stripe.Payout.list(limit=1000)
    payouts = []
    for payout in payout1['data']:
        if payout['failure_code'] == None:
            payouts.append(payout)
    morePages = True
    while morePages == True:
        lastPayoutId = payouts[-1]['id']
        newPayouts = stripe.Payout.list(limit=100,starting_after=lastPayoutId)
        if len(newPayouts) == 0:
            morePages = False
        else:
            for payout in newPayouts['data']:
                if payout['failure_code'] == None:
                    payouts.append(payout)

    transactionslist = []
    Pages = 1
    for payout in payouts:
        print('Processing page ' + str(Pages) + " of payouts")
        Pages = Pages + 1
        id = payout['id']
        transactions = stripe.BalanceTransaction.list(payout=id,limit=1000)
        for transaction in transactions:
            transactionDetails = {}
            type = transaction['type']
            if type == 'charge' or type == 'refund':
                transactionDetails['payout_id'] = id
                transactionDetails['payment_status'] = payout['status']
                amnt = float(transaction['amount'])
                transactionDetails['amount'] = str(amnt / 100)
                fee = float(transaction['fee'])
                transactionDetails['fees'] = str(fee / 100)
                transactionDetails['stripe_id'] = transaction['source']
                transactionDetails['payment_time'] = datetime.utcfromtimestamp(transaction['created']).strftime('%Y-%m-%d')
                transactionDetails['arrival_date'] = datetime.utcfromtimestamp(payout['arrival_date']).strftime('%Y-%m-%d')
                transactionslist.append(transactionDetails)
    saveDictToFile(transactionslist, 'reconciliation/extracts/stripe_transactions')



def generateStripePayoutsExtract():
    getPayouts()
