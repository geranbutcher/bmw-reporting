import stripe
from helpers.saveDictToFile import *
from pprint import pprint
from datetime import datetime

def getChargesAndRefunds():
    stripe.api_key = 'sk_live_bDhfHal81HpiY8D19OrkaJ4A'

    charge1 = stripe.Charge.list(limit=100)
    charges = []
    for charge in charge1['data']:
        if charge['failure_code'] == None:
            charges.append(charge)

    morePages = True
    Pages = 1
    while morePages == True:
        print('Getting page ' + str(Pages) + " of charges")
        Pages = Pages + 1
        lastChargeId = charges[-1]['id']
        newCharges = stripe.Charge.list(limit=100,starting_after=lastChargeId)
        if len(newCharges) == 0:
            morePages = False
        else:
            for charge in newCharges['data']:
                if charge['failure_code'] == None:
                    charges.append(charge)

    transactionslist = []
    for charge in charges:
        transactionDetails = {}
        amnt = float(charge['amount'])
        transactionDetails['amount'] = str(amnt / 100)
        transactionDetails['stripe_id'] = charge['id']
        transactionDetails['payment_time'] = datetime.utcfromtimestamp(charge['created']).strftime('%Y-%m-%d')
        transactionslist.append(transactionDetails)

    refunds1 = stripe.Refund.list(limit=100)
    refunds = []
    for refund in refunds1['data']:
        if refund['status'] == 'succeeded':
            refunds.append(refund)

    morePages = True
    Pages = 1
    while morePages == True:
        print('Getting page ' + str(Pages) + " of refunds")
        Pages = Pages + 1
        lastRefundId = refunds[-1]['id']
        newRefunds = stripe.Refund.list(limit=100,starting_after=lastRefundId)
        if len(newRefunds) == 0:
            morePages = False
        else:
            for refund in newRefunds['data']:
                if refund['status'] == 'succeeded':
                    refunds.append(refund)
    for refund in refunds:
        transactionDetails = {}
        amnt = float(refund['amount']) * -1
        transactionDetails['amount'] = str(amnt / 100)
        transactionDetails['stripe_id'] = refund['id']
        transactionDetails['payment_time'] = datetime.utcfromtimestamp(refund['created']).strftime('%Y-%m-%d')
        transactionslist.append(transactionDetails)

    saveDictToFile(transactionslist, 'reconciliation/extracts/stripe_charges')



def generateStripeTransactionsExtract():
    getChargesAndRefunds()
