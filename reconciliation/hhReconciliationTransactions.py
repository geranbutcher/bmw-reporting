from helpers.hhClient import *
from pprint import pprint
from helpers.premiumCalculation import *
from helpers.saveDictToFile import *
from datetime import *
import pymongo
import csv

def getPaymentSchedule(policy, type):
    if type == 'inception':
        if policy['eventData']['quoteData']['prices'][0]['singlePaymentSelected'] == False:
            monthlyYearly = 'MONTHLY'
        elif policy['eventData']['quoteData']['prices'][0]['singlePaymentSelected'] == True:
            monthlyYearly = 'ANNUAL'
    elif type == 'mta':
        if policy['eventData']['policyData']['singlePaymentSelected'] == False:
            monthlyYearly = 'MONTHLY'
        elif policy['eventData']['policyData']['singlePaymentSelected'] == True:
            monthlyYearly = 'ANNUAL'
    elif type == 'cancellation':
        if policy['policyData']['singlePaymentSelected'] == False:
            monthlyYearly = 'MONTHLY'
        elif policy['policyData']['singlePaymentSelected'] == True:
            monthlyYearly = 'ANNUAL'
    return monthlyYearly

def policyInceptedDetails():
    policies = getPolicyInceptions()
    allPolicyInceptedDetails = []
    for policy in policies:
        eventDetails = {}
        monthlyYearly = getPaymentSchedule(policy, 'inception')

        if (policy['eventData']['policyEndDate'] - policy['eventData']['policyStartDate']).days <= 7.1:
            totalPremium = 0
            ipt = 0
        else:
            if monthlyYearly == 'MONTHLY':
                totalPremium = getTotalPremiumReconciliation(policy, 'policy', 'inceptionMonthly')
            elif monthlyYearly == 'ANNUAL':
                totalPremium = getTotalPremiumReconciliation(policy, 'policy', 'inceptionAnnual')
            ipt = calcIPT(totalPremium)
        grossPremium = calcGrossPremium(totalPremium, ipt)
        commission = calcCommission(grossPremium)
        netPremium = calcNetPremium(grossPremium, commission)

        eventDetails['legalCoverPremium'] = getLegalCover(policy['eventData']['quoteData']['options'])
        eventDetails['stripe_charge'] = policy['eventData']['paymentAuthCode']
        eventDetails['quote_id'] = policy['eventData']['quoteReferenceId']
        eventDetails['partner_name'] = 'Wrisk'
        eventDetails['product_id'] = 'WriskMiniBMW'
        eventDetails['product_name'] = 'BMW 1.0'
        eventDetails['transaction_id'] = str(policy['_id'])
        eventDetails['policy_id'] = policy['eventData']['quoteReferenceId']
        eventDetails['pol_term_no'] = 1
        eventDetails['pol_term_seq_no'] = None
        eventDetails['transaction_stored'] = policy['eventData']['createdDate'].strftime('%Y-%m-%d')
        eventDetails['transaction_effective'] = str(policy['eventData']['policyStartDate'])
        eventDetails['transaction_expiry'] = str(policy['eventData']['policyEndDate'])
        eventDetails['transaction_sequence'] = None
        eventDetails['transaction_type'] = 'NEW BUSINESS'
        eventDetails['policy_inception'] = str(policy['eventData']['policyStartDate'])
        eventDetails['policy_expiry'] = str(policy['eventData']['policyEndDate'])
        eventDetails['previous_policy_id'] = None
        eventDetails['currency'] = 'GBP'
        eventDetails['total_premium'] = '%.2f' % totalPremium
        eventDetails['gross_premium'] = '%.2f' % grossPremium
        eventDetails['net_premium'] = '%.2f' % netPremium
        eventDetails['total_taxes'] = ipt
        eventDetails['total_commissions'] = '%.2f' % commission
        eventDetails['payment_frequency'] = monthlyYearly
        eventDetails['class_of_business'] = 'Motor'
        allPolicyInceptedDetails.append(eventDetails)
    return allPolicyInceptedDetails

def policyMTADetails():
    policies = getPoliciesAmended()
    allPolicyInceptedDetails = []
    for policy in policies:
        eventDetails = {}

        policyState = getLastPolicyState(policy)
        monthlyYearly = getPaymentSchedule(policy, 'mta')

        totalPremium = getTotalPremiumReconciliation(policy, 'policy', 'mta')
        ipt = calcIPT(totalPremium)
        grossPremium = calcGrossPremium(totalPremium, ipt)
        commission = calcCommission(grossPremium)
        netPremium = calcNetPremium(grossPremium, commission)

        eventDetails['legalCoverPremium'] = 0
        eventDetails['stripe_charge'] = None
        eventDetails['quote_id'] = policy['eventData']['quoteReferenceId']
        eventDetails['partner_name'] = 'Wrisk'
        eventDetails['product_id'] = 'WriskMiniBMW'
        eventDetails['product_name'] = 'BMW 1.0'
        eventDetails['transaction_id'] = str(policy['_id'])
        eventDetails['policy_id'] = policy['eventData']['quoteReferenceId']
        eventDetails['pol_term_no'] = 1
        eventDetails['pol_term_seq_no'] = None
        eventDetails['transaction_stored'] = policy['eventData']['createdDate'].strftime('%Y-%m-%d')
        eventDetails['transaction_effective'] = str(policy['eventData']['policyStartDate'])
        eventDetails['transaction_expiry'] = str(policy['eventData']['policyEndDate'])
        eventDetails['transaction_sequence'] = None
        eventDetails['transaction_type'] = 'MID-TERM ADJUSTMENT'
        eventDetails['policy_inception'] = str(policyState['originalPolicyStartDate'])
        eventDetails['policy_expiry'] = str(policyState['policyEndDate'])
        eventDetails['previous_policy_id'] = None
        eventDetails['currency'] = 'GBP'
        eventDetails['total_premium'] = totalPremium
        eventDetails['gross_premium'] = grossPremium
        eventDetails['net_premium'] = netPremium
        eventDetails['total_taxes'] = ipt
        eventDetails['total_commissions'] = commission
        eventDetails['payment_frequency'] = monthlyYearly
        eventDetails['class_of_business'] = 'Motor'
        allPolicyInceptedDetails.append(eventDetails)
    return allPolicyInceptedDetails

def policyCancellationDetails():
    policies = getPoliciesCancelled()
    allPolicyInceptedDetails = []
    for policy in policies:
        if policy['eventData']['refundAmount'] > 0:

            eventDetails = {}
            policyState = getLastPolicyState(policy)
            monthlyYearly = getPaymentSchedule(policyState, 'cancellation')

            if monthlyYearly == 'MONTHLY':
                totalPremium = getTotalPremiumReconciliation(policy, 'policy', 'cancellationMonthly', doc2=policyState)
            elif monthlyYearly == 'ANNUAL':
                totalPremium = getTotalPremiumReconciliation(policy, 'policy', 'cancellationAnnual', doc2=policyState)

            ipt = calcIPT(totalPremium)
            grossPremium = calcGrossPremium(totalPremium, ipt)
            commission = calcCommission(grossPremium)
            netPremium = calcNetPremium(grossPremium, commission)

            eventDetails['legalCoverPremium'] = getLegalCover(policyState['policyData']['options']) * -1
            eventDetails['stripe_charge'] = None
            eventDetails['quote_id'] = policy['eventData']['quoteReferenceId']
            eventDetails['partner_name'] = 'Wrisk'
            eventDetails['product_id'] = 'WriskMiniBMW'
            eventDetails['product_name'] = 'BMW 1.0'
            eventDetails['transaction_id'] = str(policy['_id'])
            eventDetails['policy_id'] = policy['eventData']['quoteReferenceId']
            eventDetails['pol_term_no'] = 1
            eventDetails['pol_term_seq_no'] = None
            eventDetails['transaction_stored'] = policy['eventData']['createdDate'].strftime('%Y-%m-%d')
            eventDetails['transaction_effective'] = str(policy['eventData']['cancellationDate'])
            eventDetails['transaction_expiry'] = str(policy['eventData']['cancellationDate'])
            eventDetails['transaction_sequence'] = None
            eventDetails['transaction_type'] = 'CANCELLATION'
            eventDetails['policy_inception'] = str(policyState['originalPolicyStartDate'])
            eventDetails['policy_expiry'] = str(policyState['policyEndDate'])
            eventDetails['previous_policy_id'] = None
            eventDetails['currency'] = 'GBP'
            eventDetails['total_premium'] = totalPremium
            eventDetails['gross_premium'] = grossPremium
            eventDetails['net_premium'] = netPremium
            eventDetails['total_taxes'] = ipt
            eventDetails['total_commissions'] = commission
            eventDetails['payment_frequency'] = monthlyYearly
            eventDetails['class_of_business'] = 'Motor'
            allPolicyInceptedDetails.append(eventDetails)
    return allPolicyInceptedDetails

def getAllPolicyTransactions():
    transactions = []
    inceptions = policyInceptedDetails()
    mtas = policyMTADetails()
    cancellations = policyCancellationDetails()
    transactions.extend(inceptions)
    transactions.extend(mtas)
    transactions.extend(cancellations)
    return transactions

def generateHHReconciliationExtract():
    transactions = getAllPolicyTransactions()
    saveDictToFile(transactions, 'reconciliation/extracts/hh-transactions')
